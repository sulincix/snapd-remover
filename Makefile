DESTDIR=/
build:
	echo "#!/bin/bash" > dummy.sh
	echo "exit 0" >> dummy.sh
	chmod +x dummy.sh
clean:
	rm -f dummy.sh
install:
	mkdir -p $(DESTDIR)/usr/bin/
	install dummy.sh $(DESTDIR)/usr/bin/snap
	install dummy.sh $(DESTDIR)/usr/bin/snapctl
	install dummy.sh $(DESTDIR)/usr/bin/snapfuse
	install dummy.sh $(DESTDIR)/usr/bin/ubuntu-core-launcher
